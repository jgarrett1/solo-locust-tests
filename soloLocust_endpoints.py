""" Run with:
 locust -f soloLocust_endpoints.py
 and then navigate to http://localhost:8089/ for the Locust UI
"""

from locust import HttpLocust, TaskSet, task

class MyTaskSet(TaskSet):

    """ Tests the Healthcheck. This should always succeed."""
    @task(1)
    def testHealthcheck(self):

        headers = {'Authorization': 'Basic VEVTVF9DTElFTlQ6dGVzdA=='}
        self.client.get("/private/healthcheck/live",  headers=headers)

    """ Tests the /test/success endpoint. This should always succeed."""
    @task(1)
    def testSuccess(self):

        headers = {'Authorization': 'Basic VEVTVF9DTElFTlQ6dGVzdA=='}
        self.client.post("/test/success",  headers=headers)

    """ Tests the /test/badrequest endpoint. This should always fail."""
    @task(1)
    def testBadRequest(self):

        headers = {'Authorization': 'Basic VEVTVF9DTElFTlQ6dGVzdA=='}
        self.client.post("/test/badrequest",  headers=headers)

    """ Tests /test/internalservererror. Should always return a 403."""
    @task(1)
    def testInternalServerError(self):

        headers = {'Authorization': 'Basic VEVTVF9DTElFTlQ6dGVzdA=='}
        self.client.post("/test/internalservererror", headers=headers)


class MyLocust(HttpLocust):
    task_set = MyTaskSet
    min_wait = 5000
    max_wait = 15000
    host = 'https://solo.sandbox.stage.indeed.net'